import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { UserService } from './services/user.service';
import { User } from './models/user.interface';

describe('AppComponent', () => {
  let n = 0;
  let appComponent: AppComponent;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      providers: [UserService],
      imports: [HttpClientTestingModule],
    }).compileComponents();
    userService = TestBed.inject(UserService);
    appComponent = new AppComponent(userService);
    n++;
    console.log(`beforeEach(): ${n} veces, antes de cada it`);
  }));

  beforeAll(() => {
    console.log('beforeAll(): 1 sola vez, antes de todos los its');
  });
  afterEach(() => {
    console.log(`afterEach(): ${n} veces, después de cada it`);
  });
  afterAll(() => {
    console.log('afterAll(): 1 sola vez, después de todos los its');
  });

  it('should create the app', () => {
    // const fixture = TestBed.createComponent(AppComponent);
    // const app = fixture.componentInstance;
    expect(appComponent).toBeTruthy();
    console.log(`it nº ${n}`);
  });

  it(`should have as title 'UnitTestingAngular'`, () => {
    // const fixture = TestBed.createComponent(AppComponent);
    // const app = fixture.componentInstance;
    expect(appComponent.title).toEqual('UnitTestingAngular');
    console.log(`it nº ${n}`);
  });

  it('La variable name debe ser JuanLuna ', () => {
    const saludo = appComponent.saludo;
    expect(saludo).toContain('Buenos días');
    console.log(`it nº ${n}`);
  });
  it('La funcion par() con el dato recibido 45, debe retornar false', () => {
    const par = appComponent.par(45);
    expect(par).toBeFalsy();
    console.log(`it nº ${n}`);
  });
  it('devolver un mensaje -El dato pasado debe ser un número entero- si pasamos otra cosa', () => {
    const par = appComponent.par('otra cosa');
    expect(par).toContain('El dato pasado debe ser un número entero');
    console.log(`it nº ${n}`);
  });
  it('debe llamar a nuestro servicio UserService y al método getAll(), para obtener todos los usuarios', () => {
    const mockUser: User[] = [
      {
        login: 'mojombo',
        id: 1,
        node_id: 'MDQ6VXNlcjE=',
        avatar_url: 'https://avatars0.githubusercontent.com/u/1?v=4',
        gravatar_id: '',
        url: 'https://api.github.com/users/mojombo',
        html_url: 'https://github.com/mojombo',
        followers_url: 'https://api.github.com/users/mojombo/followers',
        following_url:
          'https://api.github.com/users/mojombo/following{/other_user}',
        gists_url: 'https://api.github.com/users/mojombo/gists{/gist_id}',
        starred_url:
          'https://api.github.com/users/mojombo/starred{/owner}{/repo}',
        subscriptions_url: 'https://api.github.com/users/mojombo/subscriptions',
        organizations_url: 'https://api.github.com/users/mojombo/orgs',
        repos_url: 'https://api.github.com/users/mojombo/repos',
        events_url: 'https://api.github.com/users/mojombo/events{/privacy}',
        received_events_url:
          'https://api.github.com/users/mojombo/received_events',
        type: 'User',
        site_admin: false,
      },
      {
        login: 'defunkt',
        id: 2,
        node_id: 'MDQ6VXNlcjI=',
        avatar_url: 'https://avatars0.githubusercontent.com/u/2?v=4',
        gravatar_id: '',
        url: 'https://api.github.com/users/defunkt',
        html_url: 'https://github.com/defunkt',
        followers_url: 'https://api.github.com/users/defunkt/followers',
        following_url:
          'https://api.github.com/users/defunkt/following{/other_user}',
        gists_url: 'https://api.github.com/users/defunkt/gists{/gist_id}',
        starred_url:
          'https://api.github.com/users/defunkt/starred{/owner}{/repo}',
        subscriptions_url: 'https://api.github.com/users/defunkt/subscriptions',
        organizations_url: 'https://api.github.com/users/defunkt/orgs',
        repos_url: 'https://api.github.com/users/defunkt/repos',
        events_url: 'https://api.github.com/users/defunkt/events{/privacy}',
        received_events_url:
          'https://api.github.com/users/defunkt/received_events',
        type: 'User',
        site_admin: false,
      },
    ];
    const users = spyOn(userService, 'getAll').and.callFake(() => {
      return of(mockUser);
    });
    appComponent.ngOnInit();
    expect(users).toHaveBeenCalled();
  });
});
