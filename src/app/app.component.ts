import { Component } from '@angular/core';

import { User } from './models/user.interface';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'UnitTestingAngular';
  saludo = 'Buenos días JuanLuna!';
  users: User[] = [];

  constructor(private _userService: UserService) {}
  ngOnInit(): void {
    this.getUsers();
  }

  par(numero: any): any {
    if (isNaN(numero) || numero % 1 != 0) {
      return 'El dato pasado debe ser un número entero';
    }
    return numero % 2 === 0 ? true : false;
  }

  getUsers(): void {
    this._userService.getAll().subscribe((users) => {
      this.users = users;
      console.log(this.users);
    });
  }
}
